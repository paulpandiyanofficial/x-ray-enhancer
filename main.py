import cv2
import numpy as np

def noise_reduction(image):
    # Apply Gaussian blur for noise reduction
    blurred_image = cv2.GaussianBlur(image, (5, 5), 0)
    return blurred_image

def contrast_enhancement(image):
    # Apply histogram equalization for contrast enhancement
    equalized_image = cv2.equalizeHist(image)
    return equalized_image

def sharpening(image):
    # Apply unsharp masking for sharpening
    blurred = cv2.GaussianBlur(image, (0,0), 3)
    sharpened = cv2.addWeighted(image, 1.5, blurred, -0.5, 0)
    return sharpened

# Load the 'images.jpeg' image from the same folder
image_path = 'download.jpg'
xray_image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

# Apply preprocessing steps
xray_image_noise_reduced = noise_reduction(xray_image)
xray_image_contrast_enhanced = contrast_enhancement(xray_image_noise_reduced)
xray_image_sharpened = sharpening(xray_image_contrast_enhanced)

# Display the original and processed images
cv2.imshow('Original X-ray', xray_image)
cv2.imshow('Noise Reduced', xray_image_noise_reduced)
cv2.imshow('Contrast Enhanced', xray_image_contrast_enhanced)
cv2.imshow('Sharpened', xray_image_sharpened)
cv2.waitKey(0)
cv2.destroyAllWindows()
