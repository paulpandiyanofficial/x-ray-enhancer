import cv2
import numpy as np
import matplotlib.pyplot as plt

def apply_gaussian_blur(image, kernel_size=(3, 3), sigma=1):
    blurred_image = cv2.GaussianBlur(image, kernel_size, sigma)
    return blurred_image

def enhance_contrast(image):
    if len(image.shape) > 2:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    equalized_image = cv2.equalizeHist(image)
    return equalized_image

# def apply_unsharp_mask(image, sigma=1.0, strength=1.0):
#     blurred_image = cv2.GaussianBlur(image, (0, 0), sigma)
#     unsharp_mask = image - blurred_image
#     sharpened_image = image + strength * unsharp_mask
#     sharpened_image = cv2.normalize(sharpened_image, None, 0, 255, cv2.NORM_MINMAX)
#     return sharpened_image.astype(np.uint8)
def sharpening(image):
    # Apply unsharp masking for sharpening
    blurred = cv2.GaussianBlur(image, (0,0), 3)
    sharpened = cv2.addWeighted(image, 1.5, blurred, -0.5, 0)
    return sharpened
# Load an example X-ray image
image_path = 'download.jpg'
xray_image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

# Apply Gaussian blur for noise reduction
noise_reduced_xray_image = apply_gaussian_blur(xray_image, kernel_size=(3, 3), sigma=1)

# Enhance contrast using histogram equalization
contrast_enhanced_xray_image = enhance_contrast(noise_reduced_xray_image)

# Apply unsharp masking for sharpening
sharpened_xray_image = sharpening(contrast_enhanced_xray_image)
# Display the results
plt.figure(figsize=(20, 5))

plt.subplot(1, 4, 1)
plt.imshow(xray_image, cmap='gray')
plt.title('Original X-ray')

plt.subplot(1, 4, 2)
plt.imshow(noise_reduced_xray_image, cmap='gray')
plt.title('Noise Reduced X-ray')

plt.subplot(1, 4, 3)
plt.imshow(contrast_enhanced_xray_image, cmap='gray')
plt.title('Contrast Enhanced X-ray')

plt.subplot(1, 4, 4)
plt.imshow(sharpened_xray_image, cmap='gray')
plt.title('Sharpened X-ray')

plt.show()
