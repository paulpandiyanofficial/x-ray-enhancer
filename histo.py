import cv2
import matplotlib.pyplot as plt

def apply_gaussian_blur(image, kernel_size=(3, 3), sigma=1):
    blurred_image = cv2.GaussianBlur(image, kernel_size, sigma)
    return blurred_image

def enhance_contrast(image):
    # Convert to grayscale if the image is in color
    if len(image.shape) > 2:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Apply histogram equalization
    equalized_image = cv2.equalizeHist(image)
    
    return equalized_image

# Load an example X-ray image
image_path = 'blur_output.jpeg'
xray_image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

# Apply Gaussian blur
blurred_xray_image = apply_gaussian_blur(xray_image, kernel_size=(3, 3), sigma=1)

# Enhance contrast using histogram equalization
contrast_enhanced_image = enhance_contrast(blurred_xray_image)

# Display the results
plt.figure(figsize=(15, 5))

plt.subplot(1, 3, 1)
plt.imshow(xray_image, cmap='gray')
plt.title('Original X-ray Image')

plt.subplot(1, 3, 2)
plt.imshow(blurred_xray_image, cmap='gray')
plt.title('X-ray Image with Gaussian Blur')

plt.subplot(1, 3, 3)
plt.imshow(contrast_enhanced_image, cmap='gray')
plt.title('Contrast Enhanced Image')

plt.show()
