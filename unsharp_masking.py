import cv2
import numpy as np
import matplotlib.pyplot as plt

def enhance_contrast(image):
    # Convert to grayscale if the image is in color
    if len(image.shape) > 2:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Apply histogram equalization
    equalized_image = cv2.equalizeHist(image)
    
    return equalized_image

# def apply_unsharp_mask(image, sigma=1.0, strength=1.5):
#     blurred_image = cv2.GaussianBlur(image, (0, 0), sigma)
#     unsharp_mask = image - blurred_image
#     sharpened_image = image + strength * unsharp_mask
#     sharpened_image = np.clip(sharpened_image, 0, 255)
#     return sharpened_image.astype(np.uint8)
def sharpening(image):
    # Apply unsharp masking for sharpening
    blurred = cv2.GaussianBlur(image, (0,0), 3)
    sharpened = cv2.addWeighted(image, 1.5, blurred, -0.5, 0)
    return sharpened
# Load an example X-ray image
image_path = 'histo_output.jpeg'
xray_image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

# Enhance contrast using histogram equalization
equalized_xray_image = enhance_contrast(xray_image)

# Apply unsharp masking to the equalized image
sharpened_xray_image = sharpening(equalized_xray_image)

# Display the results
plt.figure(figsize=(15, 5))

plt.subplot(1, 3, 1)
plt.imshow(equalized_xray_image, cmap='gray')
plt.title('Contrast Enhanced Image')

plt.subplot(1, 3, 2)
plt.imshow(xray_image - equalized_xray_image, cmap='gray')
plt.title('Difference: Original - Enhanced')

plt.subplot(1, 3, 3)
plt.imshow(sharpened_xray_image, cmap='gray')
plt.title('Sharpened Image after Histogram Equalization')

plt.show()
