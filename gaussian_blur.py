import cv2
import matplotlib.pyplot as plt

def apply_gaussian_blur(image, kernel_size=(3, 3), sigma=1):
    blurred_image = cv2.GaussianBlur(image, kernel_size, sigma)
    return blurred_image

# Load an example X-ray image
image_path = 'download.jpg'
xray_image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

# Apply Gaussian blur
blurred_xray_image = apply_gaussian_blur(xray_image, kernel_size=(3, 3), sigma=1)

# Display the results
plt.figure(figsize=(10, 5))

plt.subplot(1, 2, 1)
plt.imshow(xray_image, cmap='gray')
plt.title('Original X-ray Image')

plt.subplot(1, 2, 2)
plt.imshow(blurred_xray_image, cmap='gray')
plt.title('X-ray Image with Gaussian Blur')

plt.show()
